var express = require("express");
var app = express();

app.use(express.static("public"));
app.set("view engine", "ejs");

app.get("/", function(req, res){
  res.render("home");
    // res.render("01-atoms");
});

app.get("/atoms", function(req, res){
  var colors = [
    {color: "brand-primary-l"},
    {color: "brand-primary-d"},
    {color: "brand-secondary-l"},
    {color: "brand-secondary-d"},
    {color: "brand-thrird-l"},
    {color: "brand-thrird-d"},
    {color: "brand-fourth-l"},
    {color: "brand-fourth-d"},
    {color: "base-dark"},
    {color: "base-light"},
    {color: "black"},
    {color: "white"},
  ];
  res.render("01-atoms",{colors:colors});
});

app.get("*", function(req,res) {
  res.send("no page here")
});

app.listen(3000, function(){
  console.log("Server has started!!!")
});
